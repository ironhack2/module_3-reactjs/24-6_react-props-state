import { Component } from "react";

import AddToList from "./AddToList";

class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listTime: [],
            color: "black"
        }
    }

    // Cách 1:
    // addToList = () => {
    //     let time = {
    //         hour: new Date().getHours(),
    //         minute: new Date().getMinutes(),
    //         second: new Date().getSeconds()
    //     }
    //     this.setState({
    //         listTime: [...this.state.listTime, time],
    //     })
    // }

    // Cách 2:
    addToList = () => {
        let hours = new Date().getHours();
        let minutes = new Date().getMinutes();
        let seconds = new Date().getSeconds();

        let time = ` ${hours}:${minutes}:${seconds} ${hours >= 12 ? "PM." : "AM."}`;
        this.state.listTime.push(time);

        this.setState({
            listTime: this.state.listTime,
            color: `${seconds}` % 2 === 0 ? this.state.color = "blue" : this.state.color = "red"
        })
    }

    render() {
        return (
            <div>
                <AddToList listTimeProp={this.state.listTime} addToListProp={this.addToList} changeColorProp={this.state.color} />
            </div>
        )
    }
}

export default List;