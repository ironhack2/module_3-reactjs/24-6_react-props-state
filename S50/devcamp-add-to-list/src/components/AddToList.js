import { Component } from "react";

class AddToList extends Component {

    buttonClickAdd = () => {
        this.props.addToListProp();
    }

    render() {
        return (
            <div style={{textAlign: 'center'}}>
                <h4 style={{color:this.props.changeColorProp}}>List:</h4>
                <div>
                    {
                        this.props.listTimeProp.map((element, index) => {
                            // return <p key={index}> {index + 1}. {element.hour}:{element.minute} {element.second}</p>; //Cách 1

                            return <p key={index}> {index + 1}. {element}</p> //Cách 2
                        })
                    }
                </div>
                <button onClick={this.buttonClickAdd} style={{backgroundColor:"orange",fontSize:"20px",borderRadius:"4px", borderColor:"orange" }}>Add To List</button>
            </div>
        )
    }
}

export default AddToList